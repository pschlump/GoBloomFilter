package GoBloomFilter_test

// Copyright (C) Philip Schlump, 2015.
// MIT Licensed.

import (
	"testing"

	bloom "gitlab.com/pschlump/GoBloomFilter"
)

func TestBloomFilter(t *testing.T) {
	bf := bloom.NewBloomFilter(1024)
	bf.Add([]byte("Hello"))
	bf.Add([]byte("World"))
	bf.Add([]byte("Big"))
	bf.Add([]byte("Silly"))
	bf.Add([]byte("World"))
	got := bf.IsInBloomFilter([]byte("Hello"))
	if got == false {
		t.Errorf("Error, expected %v got %v\n", true, got)
	}
	got = bf.IsInBloomFilter([]byte("Oofta"))
	if got == true {
		t.Errorf("Error, expected %v got %v\n", false, got)
	}
}
