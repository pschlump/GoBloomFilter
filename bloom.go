package GoBloomFilter

// Copyright (C) Philip Schlump, 2015.
// MIT Licensed.

// See: https://brilliant.org/wiki/bloom-filter/#false-positives-analysis

import (
	"hash"
	"hash/fnv"

	"github.com/spaolacci/murmur3"
)

// Specifiy an inteface that the bloom filter must support
type BloomFilterInterface interface {
	Add(item []byte)                  // Add to the Set
	IsInBloomFilter(item []byte) bool // Return true if it is in the set
	GetNumberOfElements() int         // Return the number of elemnts in the filter
}

// BloomFilter probabilistic data structure implementation.  Returns a BloomFilterInterface
type BloomFilter struct {
	bitSet       []uint64      // The bloom-filter set of on/off bits
	nOfElements  int           // Number of elements in the filter
	sizeOfBitset int           // Size of the bloom filter bit set == ceeling(len(bitSet)/64)
	hashFuncs    []hash.Hash64 // The hash functions
}

// NewBloomFilter returns a BloomFilterInterface
func NewBloomFilter(sz int) BloomFilterInterface {
	tsz := sz/64 + 1
	// fmt.Printf("tsz = %d\n", tsz)
	return &BloomFilter{
		bitSet:       make([]uint64, tsz),
		sizeOfBitset: sz,
		nOfElements:  0,
		hashFuncs:    []hash.Hash64{fnv.New64a(), fnv.New64(), murmur3.New64()},
	}
}

// Add to the items in the bloom filter by applying the hash functions
// and setting the corresponding flag in the bitSet.
func (bf *BloomFilter) Add(item []byte) {
	for _, aHash := range bf.hashValues(item) {
		rpos := aHash % uint64(bf.sizeOfBitset)
		mp := uint(rpos) & 0x3f
		pos := rpos >> 6
		// fmt.Printf("rpos = %d   mp = %d (mask %b)   pos = %d\n", rpos, mp, 1<<mp, pos)
		bf.bitSet[pos] |= (1 << mp)
	}
	bf.nOfElements++
}

// GetNumberOfElements returns the number of items that have been
// placed in the bloom filter set.
func (bf *BloomFilter) GetNumberOfElements() int {
	return bf.nOfElements
}

// hashValues returns the set of hash values by calling multiple hash
// functions in bf.hashFuncs.
func (bf *BloomFilter) hashValues(item []byte) []uint64 {
	var result []uint64
	for _, fx := range bf.hashFuncs {
		fx.Write(item)
		result = append(result, fx.Sum64())
		fx.Reset()
	}
	return result
}

// IsInBloomFilter returns false if the `item` is not found in the bloom filter.
// True is returned if the item is found - but the item may not exist.
// This is a probabilistic true result.
func (bf *BloomFilter) IsInBloomFilter(item []byte) bool {
	for _, aHash := range bf.hashValues(item) {
		rpos := aHash % uint64(bf.sizeOfBitset)
		mp := uint(rpos) & 0x3f
		pos := rpos >> 6
		if (bf.bitSet[uint(pos)] & (1 << mp)) == 0 {
			return false
		}
	}
	return true
}
